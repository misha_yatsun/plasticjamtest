import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from './header/header.module';
import { LogoComponent } from './logo/logo.component';
import { FooterModule } from './footer/footer.module';

@NgModule({
  declarations: [LogoComponent],
  imports: [CommonModule, HeaderModule, FooterModule],
  exports: [LogoComponent],
})
export class SharedModule {}
