import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { MainRoutingModule } from './main-routing.module';
import { MainTopModule } from './main-top/main-top.module';
import { MainMiddleModule } from './main-middle/main-middle.module';
import { MainBottomModule } from './main-bottom/main-bottom.module';

@NgModule({
  declarations: [MainComponent],
  imports: [CommonModule, MainRoutingModule, MainTopModule, MainMiddleModule, MainBottomModule],
})
export class MainModule {}
