import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-learn-more-about-our-work',
  templateUrl: './learn-more-about-our-work.component.html',
  styleUrls: ['./learn-more-about-our-work.component.scss']
})
export class LearnMoreAboutOurWorkComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
