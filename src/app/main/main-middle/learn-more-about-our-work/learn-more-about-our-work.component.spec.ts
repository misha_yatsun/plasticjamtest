import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnMoreAboutOurWorkComponent } from './learn-more-about-our-work.component';

describe('LearnMoreAboutOurWorkComponent', () => {
  let component: LearnMoreAboutOurWorkComponent;
  let fixture: ComponentFixture<LearnMoreAboutOurWorkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearnMoreAboutOurWorkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnMoreAboutOurWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
