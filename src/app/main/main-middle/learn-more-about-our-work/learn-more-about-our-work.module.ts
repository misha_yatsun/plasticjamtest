import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LearnMoreAboutOurWorkComponent } from './learn-more-about-our-work.component';
import { InfoCardsComponent } from './info-cards/info-cards.component';

@NgModule({
  declarations: [LearnMoreAboutOurWorkComponent, InfoCardsComponent],
  imports: [CommonModule],
  exports: [LearnMoreAboutOurWorkComponent],
})
export class LearnMoreAboutOurWorkModule {}
