import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainMiddleComponent } from './main-middle.component';
import { LearnMoreAboutOurWorkModule } from './learn-more-about-our-work/learn-more-about-our-work.module';
import { StartManagingAppsModule } from './start-managing-apps/start-managing-apps.module';

@NgModule({
  declarations: [MainMiddleComponent],
  imports: [CommonModule, LearnMoreAboutOurWorkModule, StartManagingAppsModule],
  exports: [MainMiddleComponent],
})
export class MainMiddleModule {}
