import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartManagingAppsComponent } from './start-managing-apps.component';

describe('StartManagingAppsComponent', () => {
  let component: StartManagingAppsComponent;
  let fixture: ComponentFixture<StartManagingAppsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartManagingAppsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartManagingAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
