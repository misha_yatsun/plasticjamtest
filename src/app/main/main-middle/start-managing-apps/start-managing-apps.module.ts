import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartManagingAppsComponent } from './start-managing-apps.component';
import { LearnMoreButtonModule } from './learn-more-button/learn-more-button.module';

@NgModule({
  declarations: [StartManagingAppsComponent],
  imports: [CommonModule, LearnMoreButtonModule],
  exports: [StartManagingAppsComponent],
})
export class StartManagingAppsModule {}
