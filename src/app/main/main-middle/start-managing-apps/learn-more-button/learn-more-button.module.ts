import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LearnMoreButtonComponent } from './learn-more-button.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [LearnMoreButtonComponent],
  imports: [CommonModule, MatButtonModule],
  exports: [LearnMoreButtonComponent],
})
export class LearnMoreButtonModule {}
