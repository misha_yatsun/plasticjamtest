import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './registration.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [RegistrationComponent],
  imports: [CommonModule, MatButtonModule],
  exports: [RegistrationComponent],
})
export class RegistrationModule {}
