import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PricingAndPackagesComponent } from './pricing-and-packages.component';

@NgModule({
  declarations: [PricingAndPackagesComponent],
  imports: [CommonModule],
  exports: [PricingAndPackagesComponent],
})
export class PricingAndPackagesModule {}
