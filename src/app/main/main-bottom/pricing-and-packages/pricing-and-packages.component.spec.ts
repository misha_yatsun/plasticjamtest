import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricingAndPackagesComponent } from './pricing-and-packages.component';

describe('PricingAndPackagesComponent', () => {
  let component: PricingAndPackagesComponent;
  let fixture: ComponentFixture<PricingAndPackagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricingAndPackagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricingAndPackagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
