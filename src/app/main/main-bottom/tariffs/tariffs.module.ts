import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TariffsComponent } from './tariffs.component';
import { PurchaseNowButtonModule } from './purchase-now-button/purchase-now-button.module';

@NgModule({
  declarations: [TariffsComponent],
  imports: [CommonModule, PurchaseNowButtonModule],
  exports: [TariffsComponent],
})
export class TariffsModule {}
