import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseNowButtonComponent } from './purchase-now-button.component';

describe('PurchaseNowButtonComponent', () => {
  let component: PurchaseNowButtonComponent;
  let fixture: ComponentFixture<PurchaseNowButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseNowButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseNowButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
