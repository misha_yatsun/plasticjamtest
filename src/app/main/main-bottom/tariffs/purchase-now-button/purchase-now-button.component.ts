import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-purchase-now-button',
  templateUrl: './purchase-now-button.component.html',
  styleUrls: ['./purchase-now-button.component.scss']
})
export class PurchaseNowButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
