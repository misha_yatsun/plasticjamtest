import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchaseNowButtonComponent } from './purchase-now-button.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [PurchaseNowButtonComponent],
  imports: [CommonModule, MatButtonModule],
  exports: [PurchaseNowButtonComponent],
})
export class PurchaseNowButtonModule {}
