import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactUsButtonComponent } from './contact-us-button.component';

@NgModule({
  declarations: [ContactUsButtonComponent],
  imports: [CommonModule],
  exports: [ContactUsButtonComponent],
})
export class ContactUsButtonModule {}
