import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainBottomComponent } from './main-bottom.component';
import { PricingAndPackagesModule } from './pricing-and-packages/pricing-and-packages.module';
import { TariffsModule } from './tariffs/tariffs.module';
import { ContactUsButtonModule } from './contact-us-button/contact-us-button.module';
import { RegistrationModule } from './registration/registration.module';
import { SharedModule } from '../../shared/shared.module';
import {FooterModule} from "../../shared/footer/footer.module";

@NgModule({
  declarations: [MainBottomComponent],
  imports: [
    CommonModule,
    PricingAndPackagesModule,
    TariffsModule,
    ContactUsButtonModule,
    RegistrationModule,
    SharedModule,
    FooterModule,
  ],
  exports: [MainBottomComponent],
})
export class MainBottomModule {}
