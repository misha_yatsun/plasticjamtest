import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewStatusButtonComponent } from './view-status-button.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [ViewStatusButtonComponent],
  imports: [CommonModule, MatButtonModule],
  exports: [ViewStatusButtonComponent],
})
export class ViewStatusButtonModule {}
