import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewStatusButtonComponent } from './view-status-button.component';

describe('ViewStatusButtonComponent', () => {
  let component: ViewStatusButtonComponent;
  let fixture: ComponentFixture<ViewStatusButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewStatusButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewStatusButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
