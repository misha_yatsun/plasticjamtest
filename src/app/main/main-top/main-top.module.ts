import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainTopComponent } from './main-top.component';
import { SharedModule } from '../../shared/shared.module';
import { ViewStatusButtonModule } from './view-status-button/view-status-button.module';

@NgModule({
  declarations: [MainTopComponent],
  imports: [CommonModule, SharedModule, ViewStatusButtonModule],
  exports: [MainTopComponent],
})
export class MainTopModule {}
